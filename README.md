# Dead Simple License Manager 4 Java

**DSLM4J** is a very simple web application and API for license key management.
You can create and edit your license keys, and then your application can check key validity
via provided API.

#### Motivation

When you develop some app (desktop or on-premise web service) at some point of time you may need 
to restrict your app usage to those who has paid for it. Using license key files and distributing them manually
may become too complicated and tedious, so easier way is to provide a web API that can be used by the app to check 
validity of its license.

There are several other solutions for that purpose (e.g. http://keygen.sh), but all of them are paid closed-source applications, 
which are an overkill for a small team of enthusiastic developers doing their first sales.  

So that is why this application is created. You can install it on your own server, add support of dslm4j API to your app
and start selling your licenses. DSLM4J will be free and open-source forever.

####Features

1. CRUD operations for your license keys
2. Extend license periods, keep track of extension history
3. Email and API notification about key upcoming expiration
4. Extra payload may be attached to license key and passed to the app
5. Restrict licenses to single or limited number of installs

####What this app is NOT

1. NOT a full DRM system. It is just a simple API that answers a single question "is this license key valid".
2. NOT a 100% secure unbreakable system. Its up to you to implement using DSLM4J API in your app, and to protect this usage from being hacked. However you must understand that no license management code that runs on client-side is 
safe from being hacked. Even top enterprise  protection software like Denuvo are hacked.
3. NOT an enterprise key management system with a lot of features, license types, configurations etc. It is "dead simple", so just a basic set of features is supported.
If you want more - you should probably afford paying for a commercial one.

DSLM4J is actually created for managing licenses of https://sigtsafari.city project. So it has just a minimal
set of features that are needed for that purpose. You can submit feature requests but they will only be implemented if I have time.

Feel free to open pull-requests with new features though.

---

### How to use

#### Prerequisites

DSLM4J is a Java web application created with Play framework. So you will need:

* Java 8+ installed
* Play Framework 1.4. Do not use Play 2.x as it is totally different and incompatible with 1.x. Grab latest version from TBD
* Database for storing your license keys. Any database which is available via JDBC driver will go.

#### Running

1. Download and unzip Play Framework from https://downloads.typesafe.com/play/1.5.1/play-1.5.1.zip. 
2. Add it to your system path so that you are able to execute its main "play" command.
3. Edit conf/application.conf file. Update it with actual database connection details. It is also a good idea to update app secret with your own value.
Check Play framework configuration guide at https://www.playframework.com/documentation/1.5.x/configuration
4. Go to app directory and run "play dependencies" to make it download and install app dependency libraries

There are 2 main ways to run a Play web application:
1. Using Play own web service. Simply type "play run" and your app will be available in dev mode at http://localhost:9000 
2. Building a .war file that can be deployed into servlet container like tomcat. Check more details at https://www.playframework.com/documentation/1.5.x/deployment

#### Managing keys

1. Go to your web application URL. If you used "play run" command then it will be available at http://localhost:9000
2. Enter **default admin credentials** admin/12345
3. Don't forget to change your password to something more secure!
4. Create your first license key. Go to "License Keys" section and press a create button. There are several key data fields that
you may configure.
5. Once your key is saved you can start querying it via the API
6. Optionally you may first create a Company. Companies are ways of organizing your keys. Each Company has a name, a email address and 
and a list of its license keys. Once key is close to expiry, if it belongs to a Company - a email notification will be sent to that Company address.

Each key has following data fields:

* Key string. This is the value you will need to pass to your customer. You may enter it manually or leave it blank, in this case 
it will be automatically generated in '1234-abcd-5678' format.
* Expiration date. If not set, key will never expire.
* Payload. This is an optional arbitrary string that will be returned to the calling party when key is validated. You may store some additional information
here, like some restrictions on this key (e.g. list of program features that are unlocked by this key).
* Installation limit. If you set it, your app will need to provide an installation fingerprint to the validation API call. Only given
number of unique fingerprints will be allowed, once this count is reached all new validations with a new fingerprint values will be rejected.
A common (but not very secure) approach is to use MAC address or host name as a fingerprint, though you may want to develop a more complicated and reliable scheme. 
* List of active installs. This is a list of fingerprints that were already used with this key. You may manually remove them, e.g. when client wants to move your software
to another machine with a different fingerprint.
* Last validation date. Indicates when this key was validated last time. Can be used to track inactive keys.
* List of renewals. Once key is close to expiry you may need to renew it (maybe a client has paid for longer usage). List of renewals is tracked and 
shown on key page

---

### API

#### Query Method

Once you have created your license, you may start querying it using our dead simple API method. Just send a GET request at
`<server location>/api/validate?key=<your key>[&installationFingerprint=<your fingerprint>]`

In response you will get a JSON structure with following fields:
```json
{
        "result": 0,

        "message": "Key is valid",

        "expirationDate": 1577836800000,

        "now": 1534686012000,

        "payload": "1000"
}
```

*result* is an integer response code (see list below)

*message* is an optional string with more details about operation result

*expirationDate* shows when this key will expire. 0 means no expiration. It may be a good idea to show some notification to client if expiration date is close. It is a java timestamp (like unix timestamp but in milliseconds instead of seconds)

*now* is server timestamp

*payload* is optional string value that was assigned to this key in admin ui

#### Response Codes

You may get following codes in response:

* 0 - Key is valid
* 1 - Bad request, malformed or missing parameters
* 2 - Key does not exist
* 3 - Key has expired (in this case you will get its expirationDate)
* 4 - Install limit reached. Key has an install limit, it is already reached and your installationFingerprint is not included in allowed fingerprint list.
* 5 - Fingerprint required. Key has an install limit and you did not provide installationFingerprint parameter
* 99 - Internal error

---

### Ideas for implementing client-side code

1. Set up https for your dslm4j installation as otherwise it will be too easy to create a proxy that always responds with succsssful validation response. 
2. You may or you may not want to spend time and money creating complicated client-side code that will be hard to hack. Usually there is no point in investing too much effort in it: the most complicated code will be hacked sooner or later, while it may disturb (with bugs, slow startup or other issues) your loyal paying clients.
3. Check for `expirationDate` field and warn your client about upcoming license expiration.
4. If license validation has failed due to a network error (no network available or your dslm4j is down) you may want to provide a grace period - allow client to use your software for several days without license checks. So that your clients do not suddenly loose access to your software due to licensing server issues.


### License

App code is licensed under GPLv3 license. You can use this app for managing licenses of closed-source commercial software,
but if you make any changes to DSLM4J itself you will need to open-source them.
