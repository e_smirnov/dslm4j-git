/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package models;

import play.Play;
import play.db.jpa.GenericModel;
import play.i18n.Messages;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Basic class for storing licenses
 */
@Entity
public class LicenseKey extends GenericModel {

    /**
     * Optional owner company that will receive notifications about this key expiry
     */
    @ManyToOne
    public Company company;

    /**
     * Unique license key. Can be just any arbitrary string
     */
    @Id
    public String keyString;

    /**
     * When this key was initially created
     */
    public long creationDate;

    /**
     * When this key was validated for last time
     */
    public long lastValidationDate;

    /**
     * When this key will expire. 0 means it does not expires and works forever
     */
    public long expirationDate;

    /**
     * If this value is set, only allow a limited number of installs (which are identified by
     * machine fingerprint string passed to validation method)
     */
    public int maxInstalls;

    /**
     * Set of unique fingerprint strings which were used with this key. If key has a limited number of maxInstalls,
     * new fingerprints will be rejected once size of this set reaches maxInstalls limit
     */
    @ElementCollection
    public Set<String> activeInstalls;

    /**
     * List of renewals of this license key
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "licenseKey")
    public List<KeyRenewal> renewals;

    /**
     * Arbitrary string that is passed to app
     */
    public String payload;

    public int expirationNotificationsSent = 0;

    @Override
    public Object _key() {
        return keyString;
    }

    public String getExpirationDateString() {
        return expirationDate > 0 ? new SimpleDateFormat("yyyy-MM-dd").format(expirationDate) : Messages.get("key.never");
    }

    public String getLastUseDateString() {
        return lastValidationDate > 0 ? new SimpleDateFormat("yyyy-MM-dd").format(lastValidationDate) : Messages.get("key.never");
    }

    public String getInstallCountString() {
        return maxInstalls > 0 ? Messages.get("key.installsCount", activeInstalls.size(), maxInstalls) : Messages.get("key.unlimited");
    }

    public String getCompanyString() {
        return company != null ? company.companyName : "-";
    }

    public LicenseKey(String keyString) {
        this.keyString = keyString;
        this.creationDate = System.currentTimeMillis();
    }

    public boolean isExpired() {
        return expirationDate != 0 && expirationDate < System.currentTimeMillis();
    }

    public boolean expiresSoon() {
        if (expirationDate == 0 || isExpired()) {
            return false;
        }
        return TimeUnit.MILLISECONDS.toDays(expirationDate - System.currentTimeMillis())
                < Integer.parseInt(Play.configuration.getProperty("license_key.warnDays"));
    }

    public static List<LicenseKey> getExpiringKeys() {
        return LicenseKey.find("expirationDate > ? and expirationDate < ?",
                System.currentTimeMillis(),
                System.currentTimeMillis() + TimeUnit.DAYS.toMillis(Integer.parseInt(Play.configuration.getProperty("license_key.warnDays")))).fetch();
    }

    public static long countExpiringKeys() {
        return LicenseKey.count("expirationDate > ? and expirationDate < ?",
                System.currentTimeMillis(),
                System.currentTimeMillis() + TimeUnit.DAYS.toMillis(Integer.parseInt(Play.configuration.getProperty("license_key.warnDays"))));
    }

    public String getColorForText() {
        if (isExpired()) {
            return "red";
        } else if (expiresSoon()) {
            return "orange";
        }
        return "darkgreen";
    }
}
