/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package models;

import controllers.Security;
import play.db.jpa.GenericModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.security.SecureRandom;

@Entity
public class User extends GenericModel  {
    @Id
    public String login;

    public long registrationDate;

    public byte[] passwordHash;

    public byte[] passwordSalt;

    public User(String login, String password) {
        this.login = login;
        setPassword(password);
        registrationDate = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return login;
    }

    public void setPassword(String password) {
        if (password.isEmpty()) {
            // do not save empty passwords
            return;
        }
        passwordSalt = new byte[32];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(passwordSalt);
        passwordHash = Security.calcHash(password, passwordSalt);
    }

    @Override
    public Object _key() {
        return login;
    }
}
