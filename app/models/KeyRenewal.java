/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package models;

import controllers.Secure;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Record of a key being renewed and its expiration date extended
 */
@Entity
public class KeyRenewal extends Model {

    /**
     * Owner key
     */
    @ManyToOne
    public LicenseKey licenseKey;

    /**
     * Date when this key was renewed
     */
    public long renewalDate;

    /**
     * New expiration date set for a key
     */
    public long nextExpirationDate;

    /**
     * Reason why it is renewed. May contain link to payment information
     */
    public String reason;

    /**
     * Login of admin user who has renewed this key
     */
    public String user;

    public KeyRenewal() {
        renewalDate = System.currentTimeMillis();
    }

    public KeyRenewal(LicenseKey key, long nextExpirationDate, String reason, String user) {
        this.licenseKey = key;
        this.renewalDate = System.currentTimeMillis();
        this.nextExpirationDate = nextExpirationDate;
        this.reason = reason;
        this.user = user;
    }
}
