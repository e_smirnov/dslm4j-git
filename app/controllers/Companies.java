/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers;

import models.Company;
import models.LicenseKey;
import play.Logger;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.modules.paginate.ModelPaginator;
import play.modules.paginate.ValuePaginator;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Companies extends Controller {

    public static void list() {
        ModelPaginator<Company> paginator = new ModelPaginator<>(Company.class);
        render(paginator);
    }

    public static void create() {
        render();
    }

    public static void edit(long id) {
        Company company = Company.findById(id);
        if (company == null) {
            notFound();
        }

        ValuePaginator<LicenseKey> keyPaginator = new ValuePaginator<>(company.keys);
        render(company, keyPaginator);
    }

    public static void createImpl(
            @Required String companyName,
            @Email String contactEmail,
            Boolean sendNotifications,
            String description
    ) {
        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            create();
        }

        if (Company.count("byCompanyName", companyName) > 0) {
            Logger.warn("Company %s already exists", companyName);
            params.flash();
            Validation.addError("companyName", Messages.get("company.alreadyExists"));
            create();
        }

        Logger.info("Creating company %s", companyName);
        Company company = new Company();
        company.companyName = companyName;
        company.contactEmail = contactEmail;
        company.sendNotifications = sendNotifications;
        company.description = description;
        company.registrationDate = System.currentTimeMillis();
        company.save();

        flash.success(Messages.get("company.created", companyName));
        list();
    }

    public static void editImpl(
            @Required long id,
            @Email String contactEmail,
            Boolean sendNotifications,
            String description
    ) {
        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            edit(id);
        }

        Company company = Company.findById(id);
        if (company == null) {
            notFound();
        }

        Logger.info("Updating company %s", company.companyName);
        company.contactEmail = contactEmail;
        company.sendNotifications = sendNotifications;
        company.description = description;

        company.save();

        flash.success(Messages.get("company.updated", company.companyName));
        list();
    }

    public static void delete(long companyId) {
        Logger.info("Deleting company with id " + companyId);
        if (Company.delete("byId", companyId) > 0) {
            flash.success(Messages.get("company.deleted"));
        }
        list();
    }

}
