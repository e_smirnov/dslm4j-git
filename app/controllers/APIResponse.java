/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers;

/**
 * These classes are returned from ValidationAPI calls
 */
public class APIResponse {

    public enum ResponseCode {
        OK(0, "Key is valid"),
        BAD_REQUEST(1, "Malformed request"),
        KEY_NOT_FOUND(2, "Such key does not exist"),
        KEY_EXPIRED(3, "Key has expired"),
        INSTALL_LIMIT_REACHED(4, "No more installs available for this key"),
        FINGERPRINT_REQUIRED(5, "This key validation requires an installationFingerpint parameter"),

        INTERNAL_ERROR(99, "Internal error, please try later")
        ;
        public final int code;

        public final String message;

        ResponseCode(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }



    /**
     * Result of a key validation when key is found in db
     */
    public static class ValidationResponse {
        /**
         * Request results
         */
        public int result;

        /**
         * Additional message
         */
        public String message;

        /**
         * When does this key expires
         */
        public long expirationDate;

        /**
         * Current server time
         */
        public long now;

        /**
         * Optional payload string as provided in LicenseKey
         */
        public String payload;

        public ValidationResponse() {
            now = System.currentTimeMillis();
        }

        public ValidationResponse(ResponseCode code) {
            result = code.code;
            message = code.message;
            now = System.currentTimeMillis();
        }
    }

}
