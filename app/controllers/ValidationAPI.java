/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers;

import models.LicenseKey;
import org.apache.commons.lang.RandomStringUtils;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Controller;

/**
 * HTTP API for validating license keys
 */
public class ValidationAPI extends Controller {

    /**
     * Validates a license key.
     * @param key Unique key string
     * @param installationFingerprint Optional installation fingerprint. Is required if key has
     *                                maxInstalls option set. This should be some id unique for a single installation of
     *                                a software, e.g. MAC address of a computer it is installed on (common but not very secure practice),
     *                                or something more complicated.
     */
    public static void validateKey(
            @Required String key,
            String installationFingerprint
    ) {
        try {
            Logger.info("[%s] Checking key %s with fingerprint %s", request.remoteAddress, key, installationFingerprint);
            if (Validation.hasErrors()) {
                Logger.warn("[%s] Bad request", request.remoteAddress);
                renderJSON(new APIResponse.ValidationResponse(APIResponse.ResponseCode.BAD_REQUEST));
            }

            LicenseKey licenseKey = LicenseKey.findById(key);
            if (licenseKey == null) {
                Logger.warn("[%s] Key not found", request.remoteAddress);
                renderJSON(new APIResponse.ValidationResponse(APIResponse.ResponseCode.KEY_NOT_FOUND));
            }

            if (licenseKey.expirationDate > 0 && System.currentTimeMillis() > licenseKey.expirationDate) {
                Logger.warn("[%s] Key %s has expired", request.remoteAddress, key);
                APIResponse.ValidationResponse response = new APIResponse.ValidationResponse(APIResponse.ResponseCode.KEY_EXPIRED);
                response.expirationDate = licenseKey.expirationDate;
                renderJSON(response);
            }

            if (licenseKey.maxInstalls > 0) {
                if (installationFingerprint == null) {
                    Logger.warn("[%s] Key %s has install limit set, but no fingerprint parameter was given", request.remoteAddress, key);
                    renderJSON(new APIResponse.ValidationResponse(APIResponse.ResponseCode.FINGERPRINT_REQUIRED));
                }

                if (!licenseKey.activeInstalls.contains(installationFingerprint)) {
                    if (licenseKey.activeInstalls.size() >= licenseKey.maxInstalls) {
                        Logger.warn("[%s] Key %s already has reached its install limit, can not add fingerprint %s", request.remoteAddress, key, installationFingerprint);
                        renderJSON(new APIResponse.ValidationResponse(APIResponse.ResponseCode.INSTALL_LIMIT_REACHED));
                    } else {
                        Logger.info("[%s] Adding new fingerprint %s to key %s", request.remoteAddress, installationFingerprint, key);
                        licenseKey.activeInstalls.add(installationFingerprint);
                    }
                }
            }
            Logger.info("[%s] Key %s is valid", request.remoteAddress, key);
            licenseKey.lastValidationDate = System.currentTimeMillis();
            licenseKey.save();

            APIResponse.ValidationResponse response = new APIResponse.ValidationResponse(APIResponse.ResponseCode.OK);
            response.expirationDate = licenseKey.expirationDate;
            response.payload = licenseKey.payload;
            renderJSON(response);
        } catch (Exception ex) {
            final String errorId = RandomStringUtils.randomAlphanumeric(10);
            Logger.error(ex, "[%s]Error while checking license, error id is %s", request.remoteAddress, errorId);
            final APIResponse.ValidationResponse response = new APIResponse.ValidationResponse(APIResponse.ResponseCode.INTERNAL_ERROR);
            response.message += " Error id " + errorId;
            renderJSON(response);
        }
    }
}
