/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers;

import models.Company;
import models.KeyRenewal;
import models.LicenseKey;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.validation.Min;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.modules.paginate.ModelPaginator;
import play.mvc.Controller;
import play.mvc.With;
import utils.LicenseKeyGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@SuppressWarnings("unused")
@With(Secure.class)
public class LicenseKeys extends Controller {

    public static void list(

    ) {
        ModelPaginator<LicenseKey> paginator = new ModelPaginator<>(LicenseKey.class);
        final long expiringCount = LicenseKey.countExpiringKeys();
        render(paginator, expiringCount);
    }

    public static void create()
    {
        List<Company> companies = Company.findAll();
        flash.put("keyString", LicenseKeyGenerator.generateKey());
        flash.put("maxInstalls", 0);
        render(companies);
    }

    public static void edit(String key) {
        LicenseKey licenseKey = LicenseKey.findById(key);
        if (licenseKey == null) {
            notFound();
        }
        render(licenseKey);
    }

    public static void createImpl(
            Long companyId,
            String keyString,
            String expirationDate,
            int maxInstalls,
            String payload) {

        final boolean manualKey = StringUtils.isNotBlank(keyString);
        if (manualKey && LicenseKey.count("byKeyString", keyString) > 0) {
            Logger.warn("Key %s already exists", keyString);
            params.flash();
            Validation.addError("keyString", Messages.get("alreadyExists"));
            flash.error(Messages.get("alreadyExists"));
            create();
        }

        Company company = null;
        if (companyId != null && companyId >= 0) {
            company = Company.findById(companyId);
            if (company == null) {
                Logger.warn("Company %d not found", companyId);
                Validation.addError("companyId", Messages.get("notFound"));
                params.flash();
                Validation.keep();
                create();
            }
        }

        LicenseKey licenseKey = new LicenseKey(manualKey ? keyString : LicenseKeyGenerator.generateKey());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            licenseKey.expirationDate = StringUtils.isNotBlank(expirationDate) ?  sdf.parse(expirationDate).getTime(): 0L;
        } catch (ParseException e) {
            Logger.warn("Invalid date %s", expirationDate);
            Validation.addError("expirationDate", Messages.get("invalidDateFormat"));
        }
        licenseKey.company = company;
        licenseKey.maxInstalls = maxInstalls;
        licenseKey.payload = payload;
        Logger.info("Saving license key %s", keyString);
        licenseKey.save();
        flash.success("key.created");
        list();
    }

    public static void editImpl(
            @Required String key,
            @Min(0) int maxInstalls,
            String payload) {

        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            edit(key);
        }

        LicenseKey licenseKey = LicenseKey.findById(key);
        if (licenseKey == null) {
            notFound();
        }

        licenseKey.payload = payload;
        licenseKey.maxInstalls = maxInstalls;
        Logger.info("Updating key %s", key);
        licenseKey.save();
        flash.success(Messages.get("updated"));
        list();
    }


    public static void renew(
            @Required String key,
            @Required String nextExpirationDate,
            @Required String reason
    ) {
        long nextExpirationDateParsed = 0L;
        try {
            nextExpirationDateParsed = new SimpleDateFormat(Play.configuration.getProperty("date.format")).parse(nextExpirationDate).getTime();
        } catch (Exception e) {
            Logger.warn("Invalid date format: " + nextExpirationDate);
            Validation.addError("nextExpirationDate", Messages.get("invalidDateFormat"));
        }
        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            edit(key);
        }
        Logger.info("Renewing key %s with next date %d", key, nextExpirationDate);
        LicenseKey licenseKey = LicenseKey.findById(key);
        if (licenseKey == null) {
            notFound();
        }

        licenseKey.expirationDate = nextExpirationDateParsed;
        licenseKey.renewals.add(new KeyRenewal(
                licenseKey,
                nextExpirationDateParsed,
                reason,
                Security.connected()
        ));

        licenseKey.save();
        flash.success(Messages.get("key.renewed"));
        edit(key);
    }

    public static void deleteFingerprint(
            String key,
            String fingerprint
    ) {
        Logger.info("Deleting fingerprint %s from key %s", fingerprint, key);
        LicenseKey licenseKey = LicenseKey.findById(key);
        if (licenseKey == null) {
            notFound();
        }

        licenseKey.activeInstalls.remove(fingerprint);
        licenseKey.save();
        flash.success(Messages.get("key.fingerprintRemoved"));
        edit(key);
    }

    public static void delete(String key) {
        Logger.info("Deleting key %s", key);
        LicenseKey.delete("byKeyString", key);
        flash.success(Messages.get("key.deleted"));
        list();
    }
}
