/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers.jobs;

import controllers.Version;
import models.User;
import org.apache.log4j.spi.RootLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class StartupJob extends Job {

    private static final Logger logger = LoggerFactory.getLogger(StartupJob.class);

    private static final String defaultAdminUsername = "admin";

    private static final String defaultAdminPassword = "12345";

    @Override
    public void doJob() {

        logger.info("Starting up " + Play.configuration.getProperty("application.name") + " v{}...", Version.VERSION);

        // Check if the database is empty
        if (User.count() == 0) {
            // add superadmin user
            User user = new User(defaultAdminUsername, defaultAdminPassword);
            user.save();
        }
        
        logger.info("Startup completed");
    }
}
