/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controllers;

import models.User;
import play.Play;
import play.data.validation.Equals;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Router;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Security extends Secure.Security {
    public static boolean authenticate(String username, String password) {
        User user = User.find("byLogin", username).first();
        return user != null &&
                ((user.passwordHash == null && password.isEmpty()) || Arrays.equals(user.passwordHash, calcHash(password, user.passwordSalt)));
    }

    static boolean check(String accessRight) {
        User user = User.find("byLogin", connected()).first();
        return user != null;
    }

    static User connectedUser() {
        return User.find("byLogin", Security.connected()).first();
    }

    private static byte[] hexStringToByteArray(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Calculates a user password sha-256 hash.
     * @param s        Password string
     * @param userSalt Optional salt
     * @return Salted and hashed password bytes
     */
    public static byte[] calcHash(String s, byte[] userSalt) {
        try {
            MessageDigest instance = MessageDigest.getInstance("sha-256");
            instance.reset();
            instance.update(s.getBytes());
            if (userSalt != null) {
                instance.update(userSalt);
            }
            instance.update(hexStringToByteArray(Play.configuration.getProperty("application.secret")));
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
