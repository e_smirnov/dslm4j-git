/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package utils;

import org.apache.commons.lang.RandomStringUtils;

import java.security.SecureRandom;

public class LicenseKeyGenerator {

    private static SecureRandom secureRandom = new SecureRandom();
    /**
     * Default key is 4 digits-4 symbols - 4 symbols -4 digits
     */
    public static String generateKey() {
        return (RandomStringUtils.random(4, 0, 0, false, true, null, secureRandom) +
                "-" + RandomStringUtils.random(4, 0, 0, true, false, null, secureRandom) +
                "-" + RandomStringUtils.random(4, 0, 0, true, false, null, secureRandom) +
                "-" + RandomStringUtils.random(4, 0, 0, false, true, null, secureRandom)).toUpperCase();
    }
}
