package functional;

import com.google.gson.Gson;
import controllers.APIResponse;
import controllers.APIResponse.ResponseCode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Http;

import java.util.concurrent.ExecutionException;

public class ValidationAPITest extends BaseFunctionalTest {

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        resetStateAndLoadYaml("db/keys.yaml");
    }

    public void testInvalidKeyReturnsError() {
        Assert.assertEquals(ResponseCode.KEY_NOT_FOUND.code,
                convertResponse(GET("/api/validate?key=invalidkeystring")).result);
    }

    public void testMissingParameterReturnsBadRequest() {
        Assert.assertEquals(ResponseCode.BAD_REQUEST.code,
                convertResponse(GET("/api/validate")).result);
    }

    @Test
    public void testUnlimitedKeyReturnsOK() {
        APIResponse.ValidationResponse response = convertResponse(GET("/api/validate?key=12345"));
        Assert.assertEquals(ResponseCode.OK.code, response.result);
        Assert.assertEquals(0, response.expirationDate);
        Assert.assertEquals("test payload", response.payload);
    }

    @Test
    public void testExpiredKeysReturnsExpirationResult() {
        Assert.assertEquals(ResponseCode.KEY_EXPIRED.code,
                convertResponse(GET("/api/validate?key=11111")).result);
    }

    @Test
    public void testWorkingKeyReturnsOK() {
        APIResponse.ValidationResponse response = convertResponse(GET("/api/validate?key=22222"));
        Assert.assertEquals(ResponseCode.OK.code, response.result);
        Assert.assertEquals(2000000000000L, response.expirationDate);
    }

    @Test
    public void testLimitedKeyRequiresFingerprint() {
        Assert.assertEquals(ResponseCode.FINGERPRINT_REQUIRED.code,
                convertResponse(GET("/api/validate?key=33333")).result);
    }

    @Test
    public void testLimitedKeyReturnsOkForAlreadyRegisteredFingerprint() {
        Assert.assertEquals(ResponseCode.OK.code,
                convertResponse(GET("/api/validate?key=33333&installationFingerprint=secondpc")).result);
    }

    @Test
    public void testLimitedKeyReturnsOkForNewFingerprint() {
        Assert.assertEquals(ResponseCode.OK.code,
                convertResponse(GET("/api/validate?key=33333&installationFingerprint=thirdpc")).result);
    }

    @Test
    public void testLimitedKeyReturnsErrorWhenInstallLimitExceeded() {
        Assert.assertEquals(ResponseCode.OK.code,
                convertResponse(GET("/api/validate?key=33333&installationFingerprint=thirdpc")).result);

        Assert.assertEquals(ResponseCode.INSTALL_LIMIT_REACHED.code,
                convertResponse(GET("/api/validate?key=33333&installationFingerprint=fourthpc")).result);

    }
}
