/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package functional;

import com.google.gson.Gson;
import controllers.APIResponse;
import org.junit.Assert;
import play.cache.Cache;
import play.jobs.Job;
import play.mvc.Http;
import play.test.Fixtures;
import play.test.FunctionalTest;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public abstract class BaseFunctionalTest extends FunctionalTest {

    private Gson gson = new Gson();

    APIResponse.ValidationResponse convertResponse(Http.Response httpResponse) {
        return gson.fromJson(httpResponse.out.toString(), APIResponse.ValidationResponse.class);
    }

    static void loginAsAdmin() {
        Map<String, String> authData = new HashMap<>();
        authData.put("username", "admin");
        authData.put("password", "querty");
        FunctionalTest.POST("/secure/authenticate", authData);
    }

    static void assertIsRedirect(Http.Response response, String url) {
        Assert.assertEquals(url, response.getHeader("Location"));
        Assert.assertEquals(302, response.status.intValue());
    }

    static void assertFlashContains(Http.Response response, String flashContent) {
        Assert.assertTrue(response.cookies.containsKey("PLAY_FLASH"));
        Assert.assertTrue(response.cookies.get("PLAY_FLASH").value.contains(URLEncoder.encode(flashContent)));
    }

    void resetStateAndLoadYaml(final String... yamlConfigs) throws ExecutionException, InterruptedException {
        clearCookies();
        Cache.clear();
        // run it in a job to commit a transaction, otherwise db changes will not be visible by controller methods
        new Job() {
            @Override
            public void doJob() {
                Fixtures.deleteDatabase();
                Fixtures.loadModels(yamlConfigs);
            }
        }.now().get();

    }
}
