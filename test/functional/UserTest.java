package functional;

import models.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.cache.Cache;
import play.i18n.Messages;
import play.jobs.Job;
import play.mvc.Http;
import play.test.Fixtures;
import play.test.FunctionalTest;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class UserTest extends BaseFunctionalTest {

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        resetStateAndLoadYaml("db/users.yaml");
    }

    @Test
    public void testCanNotAccessUserListWithoutLoggingIn() {
        Http.Response response = GET("/users/list");
        Assert.assertEquals(302, response.status.intValue());
    }

    @Test
    public void testCanNotCreateUserWithNonUniqueLogin() {
        loginAsAdmin();

        Http.Response response = POST("/users/createImpl?login=admin&password=123&passwordRepeat=123");
        assertFlashContains(response, Messages.get("user.alreadyExists"));
    }

    @Test
    public void testPasswordsShallMatch() {
        loginAsAdmin();

        Http.Response response = POST("/users/createImpl?login=newuser&password=12345&passwordRepeat=123");
        assertFlashContains(response, (Messages.get("user.passwordMismatch")));
    }

    @Test
    public void testDeleteUser() {
        loginAsAdmin();

        Assert.assertEquals(2, User.count());
        final Http.Response post = POST("/users/delete?login=simpleuser");
        assertIsRedirect(post, "/users/list");
        Assert.assertEquals(1, User.count());
    }

    @Test
    public void testCanNotDeleteLastUser() {
        loginAsAdmin();

        Assert.assertEquals(2, User.count());
        assertIsRedirect(POST("/users/delete?login=simpleuser"), "/users/list");
        Http.Response response = POST("/users/delete?login=admin");
        assertFlashContains(response, Messages.get("user.cantDeleteLast"));

        Assert.assertEquals(1, User.count());
    }

    @Test
    public void testSuccessfulUserCreation() {
        loginAsAdmin();

        Http.Response response = POST("/users/createImpl?login=newuser&password=12345&passwordRepeat=12345");
        assertFlashContains(response, Messages.get("user.created"));
        Assert.assertEquals(3, User.count());
    }

    @Test
    public void testPasswordUpdate() {
        loginAsAdmin();

        Http.Response response = POST("/users/editImpl?login=admin&newPassword=777&newPasswordRepeat=777");
        assertFlashContains(response, Messages.get("user.passwordUpdated"));

        clearCookies();
        Cache.clear();

        Map<String, String> authData = new HashMap<>();
        authData.put("username", "admin");
        authData.put("password", "querty");
        Http.Response oldPass = POST("/secure/authenticate", authData);
        assertIsRedirect(oldPass, "/secure/login");

        authData.put("password", "777");
        Http.Response newPass = POST("/secure/authenticate", authData);
        assertIsRedirect(newPass, "/");
    }
}
