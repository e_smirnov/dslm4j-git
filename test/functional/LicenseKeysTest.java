/*
 *     dslm4j - Dead Simple License Manager 4 Java
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package functional;

import controllers.APIResponse;
import models.LicenseKey;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.cache.Cache;
import play.i18n.Messages;
import play.jobs.Job;
import play.mvc.Http;
import play.test.Fixtures;
import play.test.FunctionalTest;

import java.util.concurrent.ExecutionException;

public class LicenseKeysTest extends BaseFunctionalTest {


    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        resetStateAndLoadYaml("db/keys.yaml", "db/users.yaml");
    }


    @Test
    public void testCreateNonUniqueKey() {
        loginAsAdmin();

        Http.Response response = POST("/licensekeys/createImpl?keyString=12345");
        assertFlashContains(response, Messages.get("alreadyExists"));
    }

    @Test
    public void testCreateKeyAndValidateIt() {
        loginAsAdmin();

        final String key = "987654321";
        Assert.assertEquals(APIResponse.ResponseCode.KEY_NOT_FOUND.code,
                convertResponse(GET("/api/validate?key=" + key)).result);
        POST("/licensekeys/createImpl?keyString=" + key);

        Assert.assertEquals(APIResponse.ResponseCode.OK.code,
                convertResponse(GET("/api/validate?key=" + key)).result);

    }

    @Test
    public void testRenewKeyAndValidateIt() {

        final String key = "11111";
        Assert.assertEquals(APIResponse.ResponseCode.KEY_EXPIRED.code,
                convertResponse(GET("/api/validate?key=" + key)).result);

        loginAsAdmin();
        final long expirationDate = System.currentTimeMillis() + 100000;

        POST("/licensekeys/renew?nextExpirationDate=" + expirationDate + "&key=" + key + "&reason=Test");

        APIResponse.ValidationResponse response = convertResponse(GET("/api/validate?key=" + key));
        Assert.assertEquals(APIResponse.ResponseCode.OK.code, response.result);
        Assert.assertEquals(expirationDate, response.expirationDate);
    }

    @Test
    public void testRemoveFingerprintAndValidate() {
        final String key = "44444";
        Assert.assertEquals(APIResponse.ResponseCode.INSTALL_LIMIT_REACHED.code,
                convertResponse(GET("/api/validate?key=" + key + "&installationFingerprint=thirdpc")).result);

        loginAsAdmin();
        POST("/licensekeys/deleteFingerprint?fingerprint=firstpc&key=" + key);

        LicenseKey licenseKey = LicenseKey.findById(key);
        Assert.assertEquals(1, licenseKey.activeInstalls.size());

        Assert.assertEquals(APIResponse.ResponseCode.OK.code,
                convertResponse(GET("/api/validate?key=" + key + "&installationFingerprint=thirdpc")).result);

    }
}
